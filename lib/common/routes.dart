import 'package:get/get.dart';
import 'package:ideal_clone/views/home_view.dart';
import 'package:ideal_clone/views/landing_view.dart';
import 'package:ideal_clone/views/login_view.dart';
import 'package:ideal_clone/views/signup_view.dart';

class Routes {
  static final List<GetPage<dynamic>> routes = [
    GetPage(
      name: '/',
      page: () => LandingPage(),
    ),
    GetPage(name: '/homePage', page: () => HomePage()),
    GetPage(name: '/signUp', page: () => SignUp()),
    GetPage(name: '/login', page: () => Login()),
   
  ];
}
