
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

 class Constants{
static String baseUrl =
      'https://fa-evbt-test-saasfaprod1.fa.ocs.oraclecloud.com/crmRestApi/resources/latest';
      
  static String endPoint = '/resourceUsers';
  static getDeviceWidth(BuildContext context){
    return MediaQuery.of(context).size.width;
  }
 static getDeviceHeight(BuildContext context){
    return MediaQuery.of(context).size.height;
  }
   static BaseOptions networkOptions = BaseOptions(
    baseUrl: baseUrl,
    responseType: ResponseType.json,
    connectTimeout: 10000,
    receiveTimeout: 100000,
  );
}