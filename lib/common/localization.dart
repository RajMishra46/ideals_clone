import 'package:get/get.dart';


class MyTranslations extends Translations {
  @override
Map<String, Map<String, String>> get keys => {
    'en_US': {
        'shop_now': 'Shop Now',
        'add_to_cart': 'Add to Cart',
        'win': 'Win',

    },
    'hi_IN': {
       'shop_now': 'अभी खरीदें',
        'add_to_cart': 'कार्ट में जोड़ें',
        'win': 'जीत',
    }
};
}