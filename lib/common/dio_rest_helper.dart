import 'package:dio/dio.dart';

import 'dio_request.dart';



class RestHelper {
  static Future<List<Map<String, dynamic>>> getData(
      {Map<String, dynamic> queryParameters = const {},
      var query,
      required String endPoint}) async {
    Response response;

    Dio dio = await Request().getApiClient();
    response = await dio.get(
      endPoint ,
      queryParameters: queryParameters,
    );

    return (response.data["items"] as List)
        .map((e) => e as Map<String, dynamic>)
        .toList();
  }

  static Future<dynamic> postData(
      {Map<String, dynamic> queryParameters = const {},
      required String endPoint}) async {
    Response response;
    Dio dio = await Request().getApiClient();
    response = await dio.post(endPoint, queryParameters: queryParameters);
    return response.data;
  }

  static Future<Map<String, dynamic>> patchData(
      {Map<String, dynamic> queryParameters = const {},
      required String endPoint}) async {
    Response response;
    Dio dio = await Request().getApiClient();
    response = await dio.patch(endPoint, data: queryParameters);
    return response.data;
  }

  static Future<List<dynamic>> getDataFromQuery(
      {Map<String, dynamic> queryParameters = const {},
      String? entityName}) async {
    Response response;
    Dio dio = await Request().getApiClient();
    response = await dio.post("Constants.baseUrl", data: queryParameters);
    return response.data['data'][entityName];
  }
}
