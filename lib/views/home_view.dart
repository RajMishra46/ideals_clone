import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideal_clone/views/signup_view.dart';
import 'package:ideal_clone/widgets/button.dart';
import 'package:ideal_clone/widgets/card_small.dart';
import 'package:ideal_clone/widgets/carousel.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import '../common/constants.dart';
import '../widgets/card_big.dart';
import "../widgets/dropdown_field.dart";

class HomePage extends StatefulWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomeState();
}

class _HomeState extends State<HomePage> {
  PageController _pageController = PageController(viewportFraction: 1);
  ScrollController _scrollController = new ScrollController();
  Color appBarColor = Colors.transparent;
  Color headColor = Colors.white;

  List<String> images = [
    "https://images.wallpapersden.com/image/download/purple-sunrise-4k-vaporwave_bGplZmiUmZqaraWkpJRmbmdlrWZlbWU.jpg",
    "https://wallpaperaccess.com/full/2637581.jpg",
    "https://uhdwallpapers.org/uploads/converted/20/01/14/the-mandalorian-5k-1920x1080_477555-mm-90.jpg"
  ];

  List<String> data = [
    "https://images.unsplash.com/photo-1655508342579-d139e8d74bbf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80",
    "https://images.unsplash.com/photo-1655389158915-ed128af11596?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1064&q=80",
    "https://images.unsplash.com/photo-1655422701837-c1b86521c1b4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1065&q=80"
  ];

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      setState(() {
        if (_scrollController.offset > 270) {
          appBarColor = Colors.white;
          headColor = Colors.black;
        } else {
          appBarColor = Colors.transparent;
          headColor = Colors.white;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            controller: _scrollController,
            child: _selectedIndex == 0 ? Home() : Container(),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              height: Constants.getDeviceHeight(context) * 0.1,
              width: Constants.getDeviceWidth(context),
              color: appBarColor,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text("Ideals",
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: headColor)),
                  ),
                  InkWell(
                      child:
                          Container(
                            margin: EdgeInsets.only(right: 16,top: 16),
                            height: 50, width: 50, 
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                          color: Colors.red),
                          ),
                      onTap: () {
                        showCupertinoModalBottomSheet(
                          context: context,
                          builder: (context) => SignUp(),
                        );
                      }),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'Cart',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ), //
    );
  }

  Widget Home() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Carousel(
        pageController: _pageController,
        data: images,
        carouselHeight: Constants.getDeviceHeight(context) * 0.4,
        title: "2022 Ford Bronco Wildtrak",
        description: "Spend AED150 and make it yours",
        showButton: true,
        showPageDotInside: true,
        buttonTitle: "shop_now".trArgs([""]),
        onPress: () {
          debugPrint("Show now pressed");
        },
      ),
      SizedBox(height: 16,),
      Text("Offers",
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                )),
                                SizedBox(height: 16,),
      Container(
        height: 180,
        width: Constants.getDeviceHeight(context),
        child: ListView.builder(
          // Let the ListView know how many items it needs to build.

          itemCount: data.length,
          scrollDirection: Axis.horizontal,
          // Provide a builder function. This is where the magic happens.
          // Convert each item into a widget based on the type of item it is.
          itemBuilder: (context, index) {
            return CardSmall(image: data[index]);
          },
        ),
      ),
      Container(
        width: Constants.getDeviceHeight(context),
        child: ListView.builder(
          // Let the ListView know how many items it needs to build.
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: data.length,
          // Provide a builder function. This is where the magic happens.
          // Convert each item into a widget based on the type of item it is.
          itemBuilder: (context, index) {
            return CardBig(image: data[index],showTimer: index == 0?true:false,);
          },
        ),
      )
    ]);
  }
}
