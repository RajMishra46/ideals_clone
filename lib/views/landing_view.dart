import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:ideal_clone/widgets/button.dart';
import "../widgets/dropdown_field.dart";


class LandingPage extends StatefulWidget {
  const LandingPage({Key? key,}) : super(key: key);

  
  @override
  State<LandingPage> createState() => _LandingState();
}

class _LandingState extends State<LandingPage> {
  
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    // await RestHelper.postData(
    //   body: body,
    //   endPoint: EndPoints.worksheets.replaceFirst(
    //     ':id',
    //     jobCardSrNum,
    //   ),
    // ).then((value) {
    //   if (value != null) {
    //     SharedUi().showToast(Get.context!, 'Operation Created');
    //   } else {
    //     SharedUi()
    //         .showToast(Get.context!, 'Operation not Created', isError: true);
    //   }

    //   onResponse!(value == null ? null : WorkSheetApiModel.fromJson(value));
    // });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             const Text(
              'Select Your Language',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(height: 20,),
            DropDownField(hint: "Language",list: ["English","Hindi"],dropdownValue: "English",),
            Container(
              padding: EdgeInsets.all(16),
              child: CustomButton(title: "Get Started",onPress: (){
                debugPrint("here");
                   Get.offAndToNamed('/homePage');
              },),
            )
          ],
        ),
      ), // 
    );
  }
}
