



import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideal_clone/common/constants.dart';
import 'package:ideal_clone/views/login_view.dart';
import 'package:ideal_clone/widgets/button.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';



class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child:  
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(children:  [
            Text("Register",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                )),
                                SizedBox(height: 16,),
             TextField(
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'First Name',
          ),
    ),
                                SizedBox(height: 16,),

    TextField(
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Last Name',
          ),
    ),
                                SizedBox(height: 16,),

          TextField(
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Mobile Number',
          ),
    ),
                                SizedBox(height: 16,),

              TextField(
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Email',
          ),
    ),
                                SizedBox(height: 16,),

              TextField(
          obscureText: true,
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Password',
          ),
    ),
                                SizedBox(height: 16,),

   
                              CustomButton(onPress: (){
                                showCupertinoModalBottomSheet(
                          context: context,
                          builder: (context) => Login(),
                        );
                              },title: "Login",),
                                SizedBox(height: 16,),
                                

                CustomButton(onPress: (){
                  Navigator.pop(context);
                },title: "Register",)
           
            
            ]),
        ),
      
        
    );
  }
}