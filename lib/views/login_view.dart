



import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideal_clone/common/constants.dart';
import 'package:ideal_clone/widgets/button.dart';



class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child:  
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(children:  [
            Text("Login",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                )),
                                SizedBox(height: 16,),
           

              TextField(
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Email',
          ),
    ),
                                SizedBox(height: 16,),

              TextField(
          obscureText: true,
          decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Password',
          ),
    ),
                                SizedBox(height: 16,),

   
                              CustomButton(onPress: (){
                                Get.offAndToNamed('/homePage');
                              },title: "Login",),
                                SizedBox(height: 16,),
                                

                CustomButton(onPress: (){
                  Navigator.pop(context);
                },title: "Register",)
           
            
            ]),
        ),
      
        
    );
  }
}