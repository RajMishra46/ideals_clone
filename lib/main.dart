import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'common/localization.dart';
import 'common/routes.dart';
import 'views/home_view.dart';
import "views/landing_view.dart";

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  
 

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
       defaultTransition: Transition.native,
    translations: MyTranslations(),
      locale: const Locale('en', 'US'),
    builder: ((context, child) => MediaQuery(
data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
child: child!,
)
    ),
      initialRoute: "/",
      getPages: Routes.routes,
    );
  }
}
