import 'dart:ffi';

import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final TextAlign? textAlign;

  MyText({Key? key,required this.text,this.style,this.textAlign}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return 
    Container(
      alignment: Alignment.centerLeft,
      height: 10,
       child: Row(
         children: <Widget>[
            Flexible(
               child: Text(text,style:style,textAlign: textAlign,overflow: TextOverflow.ellipsis,textDirection: TextDirection.ltr, ))
                ],
        ));
      
  }
}
