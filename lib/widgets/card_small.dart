import 'dart:ffi';

import 'package:flutter/material.dart';

class CardSmall extends StatelessWidget {
  String image;
  final String? title;
  final String? description;
  CardSmall({Key? key, required this.image, this.description, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
      child: Container(
        width: 150,
        padding: EdgeInsets.symmetric(horizontal: 8),
        alignment: Alignment.center,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(24)),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.network(image, fit: BoxFit.fill, height: 80, width: 130),
              Text("Get a chance to win",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis),
              Text(
                "Ford Branco Wiltrak",
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              Text("9999 sold out of 12000",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 10,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis),
                  SizedBox(height: 5,),
                  LinearProgressIndicator(
                    value: 50,
                    
                  )
            ]),
      ),
    );
  }
}
