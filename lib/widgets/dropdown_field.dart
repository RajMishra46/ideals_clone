import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../common/constants.dart';

class DropDownField extends StatelessWidget {
  final String dropdownValue;
  final List<String> list;
  final String hint;
  DropDownField(
      {Key? key,
      required this.dropdownValue,
      required this.hint,
      required this.list})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, top: 8, right: 16),
      margin: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: BorderRadius.circular(5)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            hint,
            style: TextStyle(fontSize: 12, color: Colors.grey),
          ),
          Container(
              width: Constants.getDeviceWidth(context),
              height: 35,
              child: DropdownButtonHideUnderline(
                // to hide the default underline of the dropdown button
                child: DropdownButton<String>(
                  value: dropdownValue,
                  icon: Container(),
                  onChanged: (String? newValue) {
                    if (newValue == "English") {
                      Get.updateLocale(Locale('en', 'US'));
                    } else {
                      Get.updateLocale(Locale('hi', 'IN'));
                    }
                    
                  },
                  items: list.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: const TextStyle(fontSize: 15),
                      ),
                    );
                  }).toList(),
                ),
              )),
        ],
      ),
    );
  }
}
