import 'dart:ffi';

import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String? title;
  final Function() onPress;
  final double? width;
  final double? height;
  CustomButton({Key? key, this.title,required this.onPress,this.width,this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child:  Container(
              height: height?? 50,
              child: ElevatedButton(
                
                onPressed: () {onPress();},
                child: Text(title!),
              ),
            ),
          
        ),
      ],
    );
  }
}
