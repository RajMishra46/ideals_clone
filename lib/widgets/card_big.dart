import 'dart:async';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideal_clone/common/constants.dart';

import 'button.dart';

class CardBig extends StatefulWidget{
  String image;
  final String? title;
  final String? description;
  final bool? showTimer;
  final Duration? time;
  CardBig({Key? key, required this.image, this.description, this.title,this.showTimer,this.time})
      : super(key: key);

@override
  State<CardBig> createState() => _CardState();
}

class _CardState extends State<CardBig>{
  Timer? countdownTimer;
  Duration myDuration = Duration(days: 5);
  @override
  void initState() {
    super.initState();
    startTimer();
  }
  void startTimer() {
    countdownTimer =
        Timer.periodic(Duration(seconds: 1), (_) => setCountDown());
  }
  void setCountDown() {
    final reduceSecondsBy = 1;
    setState(() {
      final seconds = myDuration.inSeconds - reduceSecondsBy;
      if (seconds < 0) {
        countdownTimer!.cancel();
      } else {
        myDuration = Duration(seconds: seconds);
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    String strDigits(int n) => n.toString().padLeft(2, '0');
    final days = strDigits(myDuration.inDays);
    // Step 7
    final hours = strDigits(myDuration.inHours.remainder(24));
    final minutes = strDigits(myDuration.inMinutes.remainder(60));
    final seconds = strDigits(myDuration.inSeconds.remainder(60));
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
      child: Container(
          width: Constants.getDeviceWidth(context),
  

        padding: EdgeInsets.all(16),
        alignment: Alignment.center,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(24)),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
             if( widget.showTimer!) ...[
Text(
              '$hours:$minutes:$seconds',
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  
                  fontSize: 30),
            ),
             ],
              Image.network(widget.image, fit: BoxFit.fill, height: 140,width: Constants.getDeviceWidth(context)*0.9, ),
              Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                 Text("win".trArgs([""]),textAlign: TextAlign.end,style: TextStyle(color: Colors.red,fontSize: 40,fontWeight: FontWeight.bold),),
                             Text("2022 Ford Bronco Wildtrak",style:TextStyle(fontSize:20,fontWeight: FontWeight.bold),),
                        Text( "Spend AED150 and make it yours",style:TextStyle(fontSize:12,fontWeight: FontWeight.normal)),
                      
                          ],
                        ),
                         
            ]),
             Container(
                              margin: EdgeInsets.only(top: 16),
                              child:CustomButton(onPress:(){},title:"add_to_cart".trArgs([""]),) )
            ])
    ));
  }
}
