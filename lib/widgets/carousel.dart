import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:ideal_clone/widgets/button.dart';
import 'package:ideal_clone/widgets/my_text.dart';

import '../common/constants.dart';

class Carousel extends StatelessWidget {
  final String? title;
  final String? description;
  final double carouselHeight;
  final Function()? onPress;
  final bool? showButton;
  final String? buttonTitle;

  final bool? showPageDotInside;
  final bool? showPageDotOutside;

  final List data;
  final PageController pageController;
  Carousel({Key? key, this.title, this.onPress,this.showButton,this.showPageDotInside,this.showPageDotOutside,required this.data,required this.pageController,this.description,required this.carouselHeight,this.buttonTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
 height: carouselHeight,
      child: PageView.builder(
                    itemCount: data.length,
                    pageSnapping: true,
                    controller: pageController,
                    // onPageChanged: (page) {
              // setState(() {
              //   activePage = page;
              // });
                    // },
                    itemBuilder: (context, pagePosition) {
              return Container(
               height: carouselHeight,
              width:Constants.getDeviceWidth(context) ,
                child: Stack(
                  children: [
                    Image.network(data[pagePosition],height: carouselHeight,width:Constants.getDeviceWidth(context) ,fit: BoxFit.fitHeight ,),
                    Positioned(
                      bottom:10,
                      child: Container(
                        width:Constants.getDeviceWidth(context) ,
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                 Text("win".trArgs([""]),textAlign: TextAlign.end,style: TextStyle(color: Colors.red,fontSize: 40,fontWeight: FontWeight.bold),),
                             Text(title ?? "",style:TextStyle(color: Colors.white,fontSize:20,fontWeight: FontWeight.bold),),
                        Text(description ?? "",style:TextStyle(color: Colors.white,fontSize:12,fontWeight: FontWeight.normal)),
                      
                           if( showButton! && buttonTitle !=null && onPress !=null) ...[
                            Container(
                              width: 120,
                              margin: EdgeInsets.only(top: 16),
                              child:CustomButton(onPress:(){onPress!();},title:buttonTitle,height: 30,) )],
                              ],
                            ),
                             showPageDotInside! ? dots():Container()
                          ],
                        ),
                      ),
                    ),
                  
                  ],
                ),
              );
                    }),
    );
  }


  Widget dots(){
  return Container(
 child: Row(children:data.asMap().map((i, element) => MapEntry(i,
 
 Container(
      width: 8,
      height: 8,
      margin: EdgeInsets.symmetric(horizontal: 2),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color:  Colors.white),
)

)).values.toList()
 
 


    ),
  );
}

}




